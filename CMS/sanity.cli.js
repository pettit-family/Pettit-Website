import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'wnmjvc1d',
    dataset: 'production'
  }
})
