import peterAlbum from './Peter/album'
import peterPhotoWithDescription from './Peter/photoWithDescription'

export const schemaTypes = [peterAlbum, peterPhotoWithDescription]
