export default {
  name: 'peter.album',
  title: 'Peter Album',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'string',
      validation: (Rule) => Rule.required(),
    },
    {
      name: 'coverImage',
      title: 'Cover Image',
      type: 'image',
      options: {
        hotspot: true,
      },
      validation: (Rule) => Rule.required(),
    },
    {
      name: 'description',
      title: 'Description',
      type: 'text',
    },
    {
      name: 'photos',
      title: 'Photos',
      type: 'array',
      of: [{type: 'peter.photoWithDescription'}],
    },
  ],
}
