export default {
  name: 'peter.photoWithDescription',
  title: 'Photo with Description',
  type: 'object',
  fields: [
    {
      name: 'photo',
      title: 'Photo',
      type: 'image',
      options: {
        hotspot: true,
      },
      validation: (Rule) => Rule.required(),
    },
    {
      name: 'identifier',
      title: 'Identifier',
      type: 'string',
      description: 'Internal only, this is the file name in the album management page',
    },
    {
      name: 'title',
      title: 'Title',
      type: 'string',
      description: 'Optional display title for the photo',
    },
    {
      name: 'description',
      title: 'Description',
      type: 'text',
      description: 'Optional description for the photo',

    },
  ],
  preview: {
    select: {
      imageUrl: 'photo.asset.url',
      title: 'identifier',
      subtitle: 'description',
    }
  }
};
